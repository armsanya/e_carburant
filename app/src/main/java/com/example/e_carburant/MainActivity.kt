package com.example.e_carburant

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.fragment.app.FragmentContainerView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    val cities = arrayOf("Abomey-Calavi", "Abomey", "Athiémé", "Allada", "Aplahoué","Bohicon", "Banikoara", "Bassila", "Bembéréké",
        "Comè", "Covè","Cotonou", "Dassa-Zoumè", "Djougou", "Dogbo-Tota", "Grand Popo", "Kandi", "Kérou", "Kétou", "Kouandé", "Lokossa",
        "Malanville", "Natitingou", "N'Dali", "Nikki", "Ouidah", "Parakou", "Pobè", "Porto-Novo", "Sakété", "Savalou", "Savè", "Segbana",
        "Tanguiéta", "Tchaourou")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //val mapFragment = supportFragmentManager.findFragmentById(R.id.principmap) as PrincipalMapsActivity
        //mapFragment.getMapAsync(mapFragment)

        val choiceEssence = findViewById<CheckBox>(R.id.essenceBox)
        val choiceGasoil = findViewById<CheckBox>(R.id.gasoilBox)
        val buttonSearch = findViewById<Button>(R.id.searchbutton)
        val fragm = findViewById<ImageView>(R.id.principmap)
        fragm.setImageURI(Uri.parse("android.resource://"+packageName+"/"+R.raw.imagemap))
        fragm.setOnClickListener {
            val mapintent = Intent(this, PrincipalMapsActivity::class.java)
            startActivity(mapintent)
        }
        /*mapViewHome = findViewById(R.id.mapView)
        var mapviewbundle = savedInstanceState?.getBundle("MapViewBundleKey")
        mapViewHome.onCreate(mapviewbundle)

        mapViewHome.getMapAsync(this)*/

        //var carburantChoice
        var carburantChoice = emptyArray<String>()
        var citiespinner = findViewById<Spinner>(R.id.spinner)
        var cityChoosed : String
        val arrayAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, cities )
        //arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        citiespinner.adapter = arrayAdapter

        citiespinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                cityChoosed = cities[position]
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }


        buttonSearch.setOnClickListener {
            if (choiceEssence.isChecked){
                carburantChoice.plus("Essence")
            }
            if (choiceGasoil.isChecked){
                carburantChoice.plus("Gasoil")
            }

        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.principal, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId ){
            R.id.principal_item1 ->{
                val intentConnect = Intent(this, ChoixTypeCompteInscription::class.java)
                startActivity(intentConnect)
                return true
            }
            R.id.principal_item2 ->{
                val intentSignIn = Intent(this, Login::class.java)
                startActivity(intentSignIn)
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        TODO("Not yet implemented")
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }
}