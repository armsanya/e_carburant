package com.example.e_carburant

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.OpenableColumns
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class InscriptionCadre : AppCompatActivity() {
    private lateinit var nameEdit: EditText
    private lateinit var firstname: EditText
    private lateinit var emailEdit: EditText
    private lateinit var passwordEdit: EditText
    private lateinit var confirmpasswordEdit: EditText
    private lateinit var buttonFileChosing: ImageButton
    private lateinit var fileChoosedName: EditText
    private lateinit var submissionButton: Button
    private final  var REQ_SELECT_FILE = 1001

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inscription_cadre)

        nameEdit = findViewById(R.id.namecadre)
        firstname = findViewById(R.id.cadrefirstname)
        emailEdit = findViewById(R.id.emailcadre)
        passwordEdit = findViewById(R.id.passwordcadre)
        confirmpasswordEdit = findViewById(R.id.confirmpasswordcadre)
        buttonFileChosing = findViewById(R.id.imageButton)
        fileChoosedName =findViewById(R.id.nomfichierscadre)
        submissionButton =findViewById(R.id.buttonsubmitcadre)

        submissionButton.setOnClickListener {
            var nom = nameEdit.getText()
            var prenom = firstname.getText()
            var email = emailEdit.getText()
            var password = passwordEdit.getText()
            var confpassword = confirmpasswordEdit.getText()
            var filesName = fileChoosedName.getText()
        }

        buttonFileChosing.setOnClickListener {
            val choosingIntent = Intent(Intent.ACTION_OPEN_DOCUMENT)
            choosingIntent.addCategory(Intent.CATEGORY_OPENABLE)
            choosingIntent.setType("application/pdf")
            choosingIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            startActivityForResult(choosingIntent, REQ_SELECT_FILE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQ_SELECT_FILE && resultCode == RESULT_OK){
            if(data != null && data.getData() != null){
                var newFilePath = getFileInfos(data.getData()!!)
                if(newFilePath != null){
                    fileChoosedName.setText("Pas de fichier(s) choisi(s)")
                }
            }
        }
    }

    private fun  getFileInfos(fileUri : Uri) : String{
        var tabString = arrayOf(OpenableColumns.DISPLAY_NAME, OpenableColumns.SIZE)
        var cursor = getContentResolver().query(fileUri, tabString, null, null, null)
        cursor?.moveToFirst()
        var fileName = cursor?.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
        var dir = getFilesDir()
        var fileChoosen = File("$dir+/+$fileName")
        try {
            var fileoutputstream = FileOutputStream(fileChoosen)
            var inputstream = getContentResolver().openInputStream(fileUri)
            var buffers = ByteArray(1024)
            var read = inputstream?.read(buffers)
            while (read!= -1 && read != null){
                fileoutputstream.write(buffers, 0, read)
            }
            inputstream?.close()
            fileoutputstream.close()
            return fileChoosen.getPath()
        } catch (e : IOException){
            e.printStackTrace()
        }
        return ""
    }

}