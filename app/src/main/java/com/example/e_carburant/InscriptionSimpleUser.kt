package com.example.e_carburant

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class InscriptionSimpleUser : AppCompatActivity() {
    private lateinit var nameEdit: EditText
    private lateinit var emailEdit: EditText
    private lateinit var firstname: EditText
    private lateinit var passwordEdit: EditText
    private lateinit var confirmpasswordEdit: EditText
    private lateinit var submissionButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inscription_simple_user)

        nameEdit = findViewById(R.id.nameusersimple)
        firstname = findViewById(R.id.firstnameusersimple)
        emailEdit = findViewById(R.id.emailusersimple)
        passwordEdit = findViewById(R.id.passwordusersimple)
        confirmpasswordEdit = findViewById(R.id.confirmpasswordusersimple)
        submissionButton =findViewById(R.id.button5)

        submissionButton.setOnClickListener {
            var nom = nameEdit.getText()
            var prenom = firstname.getText()
            var email = emailEdit.getText()
            var password = passwordEdit.getText()
            var confpassword = confirmpasswordEdit.getText()
        }
    }
}