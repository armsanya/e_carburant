package com.example.e_carburant

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.example.e_carburant.databinding.ActivityPrincipalMapsBinding
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.Marker
import java.util.*
import java.util.jar.Manifest

class PrincipalMapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityPrincipalMapsBinding
    private lateinit var currentlocation: Location
    var fusedLocationProviderClient: FusedLocationProviderClient ? = null
    var currentMarker : Marker ? = null

    companion object{
        private const val LOCATION_REQUEST_CODE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityPrincipalMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        /*val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)*/
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        setupMap()
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings.isZoomControlsEnabled = true
        // Add a marker in Sydney and move the camera
        /*val sydney = LatLng(-34.0, 151.0)
        mMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
         mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLong, 12f))*/
        /*mMap.setOnMarkerClickListener(this)
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        setupMap()*/
        val currentLatLong = LatLng(currentlocation?.latitude!!, currentlocation?.longitude!!)
        placeMarkerOnMap(currentLatLong)
        mMap.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener {
            override fun onMarkerDrag(p0: Marker?) {
                TODO("Not yet implemented")
            }

            override fun onMarkerDragEnd(p0: Marker?) {
                if(currentMarker != null)
                    currentMarker?.remove()
                val newLatLong = LatLng(p0?.position!!.latitude, p0?.position!!.longitude)
                placeMarkerOnMap(newLatLong)
            }

            override fun onMarkerDragStart(p0: Marker?) {
                TODO("Not yet implemented")
            }
        })
    }

    @SuppressLint("MissingPermission")
    private fun setupMap(){
        if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED ){

            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_REQUEST_CODE)
            return
        }
//       mMap.isMyLocationEnabled = true
       val task = fusedLocationProviderClient?.lastLocation
        task?.addOnSuccessListener { location ->
            if(location != null){
                this.currentlocation = location
                val mapFragment = supportFragmentManager
                    .findFragmentById(R.id.map) as SupportMapFragment
                mapFragment.getMapAsync(this)
            }
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode){
            1000 -> if(grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                setupMap()
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun placeMarkerOnMap(cLatLong : LatLng){
        val markerOptions = MarkerOptions().position(cLatLong)
        markerOptions.title("$cLatLong")
            .snippet(getAddress(cLatLong.latitude, cLatLong.longitude))
        mMap.animateCamera(CameraUpdateFactory.newLatLng(cLatLong))
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(cLatLong, 85f))
        currentMarker = mMap.addMarker(markerOptions)
        currentMarker?.showInfoWindow()
    }

    private fun getAddress(lat : Double, longu : Double) : String? {
        val geocoder = Geocoder(this, Locale.getDefault())
        val addresshere = geocoder.getFromLocation(lat, longu, 1)
        return addresshere[0].getAddressLine(0).toString()
    }

    /*override fun onMarkerClick(p0: Marker?): Boolean {
        TODO("Not yet implemented")
    }*/
    override fun onMarkerClick(p0: Marker?) = false
}