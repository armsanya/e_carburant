package com.example.e_carburant

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class Login : AppCompatActivity() {
    private lateinit var emailEdit: EditText
    private lateinit var passwordEdit: EditText
    private lateinit var boutonConnect: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        boutonConnect = findViewById(R.id.button)
        passwordEdit = findViewById(R.id.editTextTextPassword)
        emailEdit = findViewById(R.id.editTextTextEmailAddress2)

        boutonConnect.setOnClickListener {
            val emailVal = emailEdit.text.toString()
            val passwordVal = passwordEdit.text.toString()
            println("emailVal=$emailVal")
            println("passwordVal=$passwordVal")
        }

    }
}