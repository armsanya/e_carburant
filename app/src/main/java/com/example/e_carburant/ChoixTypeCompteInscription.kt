package com.example.e_carburant

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import androidx.appcompat.widget.AppCompatImageButton

class ChoixTypeCompteInscription : AppCompatActivity() {
   /* private lateinit var viewstation : View
    private lateinit var viewcadre : View
    private lateinit var viewsimpleuser : View*/
    /*private lateinit var stationImageButton : ImageButton
    private lateinit var cadreImageButton : ImageButton
    private lateinit var simpleUserImageButton : ImageButton*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choix_type_compte_inscription)
        val viewstation = findViewById<View>(R.id.stationview)
        val viewcadre = findViewById<View>(R.id.personalView)
        val viewsimpleuser = findViewById<View>(R.id.simpleuserview)

       // val viewgen = findViewById<View>(R.id.view)

        viewstation.setOnClickListener {
            val intentStationInscription = Intent(this, InscriptionStation::class.java)
            startActivity(intentStationInscription)
        }

        val stationImageButton = findViewById<ImageButton>(R.id.imageButton2)
        stationImageButton.setOnClickListener {
            val intentStationInscription = Intent(this, InscriptionStation::class.java)
            startActivity(intentStationInscription)
        }

        viewcadre.setOnClickListener {
            val intentCadreInscription = Intent(this, InscriptionCadre::class.java)
            startActivity(intentCadreInscription)
        }

        val cadreImageButton = findViewById<ImageButton>(R.id.imageButton3)
        cadreImageButton.setOnClickListener {
            val intentCadreInscription = Intent(this, InscriptionCadre::class.java)
            startActivity(intentCadreInscription)
        }

        viewsimpleuser.setOnClickListener {
            val intentSimpleUserInscription= Intent(this, InscriptionSimpleUser::class.java)
            startActivity(intentSimpleUserInscription)
        }

        val simpleUserImageButton = findViewById<ImageButton>(R.id.imageButton4)
        simpleUserImageButton.setOnClickListener {
            val intentSimpleUserInscription= Intent(this, InscriptionSimpleUser::class.java)
            startActivity(intentSimpleUserInscription)
        }
    }
}