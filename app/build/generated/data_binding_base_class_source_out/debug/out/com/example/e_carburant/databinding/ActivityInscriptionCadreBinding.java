// Generated by view binder compiler. Do not edit!
package com.example.e_carburant.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.example.e_carburant.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ActivityInscriptionCadreBinding implements ViewBinding {
  @NonNull
  private final ConstraintLayout rootView;

  @NonNull
  public final Button buttonsubmitcadre;

  @NonNull
  public final EditText cadrefirstname;

  @NonNull
  public final EditText confirmpasswordcadre;

  @NonNull
  public final EditText emailcadre;

  @NonNull
  public final ImageButton imageButton;

  @NonNull
  public final EditText namecadre;

  @NonNull
  public final EditText nomfichierscadre;

  @NonNull
  public final EditText passwordcadre;

  @NonNull
  public final TextView textView17;

  @NonNull
  public final View view3;

  private ActivityInscriptionCadreBinding(@NonNull ConstraintLayout rootView,
      @NonNull Button buttonsubmitcadre, @NonNull EditText cadrefirstname,
      @NonNull EditText confirmpasswordcadre, @NonNull EditText emailcadre,
      @NonNull ImageButton imageButton, @NonNull EditText namecadre,
      @NonNull EditText nomfichierscadre, @NonNull EditText passwordcadre,
      @NonNull TextView textView17, @NonNull View view3) {
    this.rootView = rootView;
    this.buttonsubmitcadre = buttonsubmitcadre;
    this.cadrefirstname = cadrefirstname;
    this.confirmpasswordcadre = confirmpasswordcadre;
    this.emailcadre = emailcadre;
    this.imageButton = imageButton;
    this.namecadre = namecadre;
    this.nomfichierscadre = nomfichierscadre;
    this.passwordcadre = passwordcadre;
    this.textView17 = textView17;
    this.view3 = view3;
  }

  @Override
  @NonNull
  public ConstraintLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ActivityInscriptionCadreBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ActivityInscriptionCadreBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.activity_inscription_cadre, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ActivityInscriptionCadreBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.buttonsubmitcadre;
      Button buttonsubmitcadre = ViewBindings.findChildViewById(rootView, id);
      if (buttonsubmitcadre == null) {
        break missingId;
      }

      id = R.id.cadrefirstname;
      EditText cadrefirstname = ViewBindings.findChildViewById(rootView, id);
      if (cadrefirstname == null) {
        break missingId;
      }

      id = R.id.confirmpasswordcadre;
      EditText confirmpasswordcadre = ViewBindings.findChildViewById(rootView, id);
      if (confirmpasswordcadre == null) {
        break missingId;
      }

      id = R.id.emailcadre;
      EditText emailcadre = ViewBindings.findChildViewById(rootView, id);
      if (emailcadre == null) {
        break missingId;
      }

      id = R.id.imageButton;
      ImageButton imageButton = ViewBindings.findChildViewById(rootView, id);
      if (imageButton == null) {
        break missingId;
      }

      id = R.id.namecadre;
      EditText namecadre = ViewBindings.findChildViewById(rootView, id);
      if (namecadre == null) {
        break missingId;
      }

      id = R.id.nomfichierscadre;
      EditText nomfichierscadre = ViewBindings.findChildViewById(rootView, id);
      if (nomfichierscadre == null) {
        break missingId;
      }

      id = R.id.passwordcadre;
      EditText passwordcadre = ViewBindings.findChildViewById(rootView, id);
      if (passwordcadre == null) {
        break missingId;
      }

      id = R.id.textView17;
      TextView textView17 = ViewBindings.findChildViewById(rootView, id);
      if (textView17 == null) {
        break missingId;
      }

      id = R.id.view3;
      View view3 = ViewBindings.findChildViewById(rootView, id);
      if (view3 == null) {
        break missingId;
      }

      return new ActivityInscriptionCadreBinding((ConstraintLayout) rootView, buttonsubmitcadre,
          cadrefirstname, confirmpasswordcadre, emailcadre, imageButton, namecadre,
          nomfichierscadre, passwordcadre, textView17, view3);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
