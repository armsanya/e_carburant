// Generated by view binder compiler. Do not edit!
package com.example.e_carburant.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.example.e_carburant.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ActivityLoginBinding implements ViewBinding {
  @NonNull
  private final ConstraintLayout rootView;

  @NonNull
  public final Button button;

  @NonNull
  public final EditText editTextTextEmailAddress2;

  @NonNull
  public final EditText editTextTextPassword;

  @NonNull
  public final TextView textView;

  @NonNull
  public final TextView textView13;

  @NonNull
  public final TextView textView18;

  @NonNull
  public final View view2;

  private ActivityLoginBinding(@NonNull ConstraintLayout rootView, @NonNull Button button,
      @NonNull EditText editTextTextEmailAddress2, @NonNull EditText editTextTextPassword,
      @NonNull TextView textView, @NonNull TextView textView13, @NonNull TextView textView18,
      @NonNull View view2) {
    this.rootView = rootView;
    this.button = button;
    this.editTextTextEmailAddress2 = editTextTextEmailAddress2;
    this.editTextTextPassword = editTextTextPassword;
    this.textView = textView;
    this.textView13 = textView13;
    this.textView18 = textView18;
    this.view2 = view2;
  }

  @Override
  @NonNull
  public ConstraintLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ActivityLoginBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ActivityLoginBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.activity_login, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ActivityLoginBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.button;
      Button button = ViewBindings.findChildViewById(rootView, id);
      if (button == null) {
        break missingId;
      }

      id = R.id.editTextTextEmailAddress2;
      EditText editTextTextEmailAddress2 = ViewBindings.findChildViewById(rootView, id);
      if (editTextTextEmailAddress2 == null) {
        break missingId;
      }

      id = R.id.editTextTextPassword;
      EditText editTextTextPassword = ViewBindings.findChildViewById(rootView, id);
      if (editTextTextPassword == null) {
        break missingId;
      }

      id = R.id.textView;
      TextView textView = ViewBindings.findChildViewById(rootView, id);
      if (textView == null) {
        break missingId;
      }

      id = R.id.textView13;
      TextView textView13 = ViewBindings.findChildViewById(rootView, id);
      if (textView13 == null) {
        break missingId;
      }

      id = R.id.textView18;
      TextView textView18 = ViewBindings.findChildViewById(rootView, id);
      if (textView18 == null) {
        break missingId;
      }

      id = R.id.view2;
      View view2 = ViewBindings.findChildViewById(rootView, id);
      if (view2 == null) {
        break missingId;
      }

      return new ActivityLoginBinding((ConstraintLayout) rootView, button,
          editTextTextEmailAddress2, editTextTextPassword, textView, textView13, textView18, view2);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
